
if ('~/.work-tokens.nu' | path exists) {
    source ~/.work-tokens.nu
}

if ('~/.ssi/bin' | path exists)  {
  $env.PATH = ($env.PATH | split row (char esep) | append ($env.HOME | path join ".ssi" "bin"))
}

$env.PATH = ($env.PATH | split row (char esep) | append "/run/current-system/sw/bin")

$env.PATH = ($env.PATH | split row (char esep) | append "/usr/local/bin")
$env.PATH = ($env.PATH | split row (char esep) | append "/usr/local/sbin")
$env.PATH = ($env.PATH | split row (char esep) | append "/usr/local/opt/go/libexec/bin")

$env.PATH = ($env.PATH | split row (char esep) | append "/opt/homebrew/sbin")
$env.PATH = ($env.PATH | split row (char esep) | append "/opt/homebrew/bin")
$env.PATH = ($env.PATH | split row (char esep) | append "/opt/homebrew/opt/libpq/bin")
$env.PATH = ($env.PATH | split row (char esep) | append "/opt/homebrew/libiconv/bin")
$env.PATH = ($env.PATH | split row (char esep) | append "/opt/homebrew/opt/curl/bin")
$env.PATH = ($env.PATH | split row (char esep) | append "/opt/homebrew/opt/mysql-client/bin")

$env.PATH = ($env.PATH | split row (char esep) | append ($env.HOME | path join ".asdf" "shims"))
$env.PATH = ($env.PATH | split row (char esep) | append ($env.HOME | path join ".local" "bin"))

$env.PATH = ($env.PATH | split row (char esep) | append ($env.HOME | path join ".config" "emacs" "bin"))

$env.PATH = ($env.PATH | split row (char esep) | append ($env.HOME | path join ".cargo" "bin"))
$env.PATH = ($env.PATH | split row (char esep) | append ($env.HOME | path join ".ghcup" "bin"))
$env.PATH = ($env.PATH | split row (char esep) | append ($env.HOME | path join ".cabal" "bin"))
$env.PATH = ($env.PATH | split row (char esep) | append ($env.HOME | path join "go" "third" "bin"))
$env.PATH = ($env.PATH | split row (char esep) | append ($env.HOME | path join ".pyenv" "shims"))

$env.MYSQL_CLIENT_BIN = '/opt/homebrew/opt/mysql-client/bin'
$env.JAVA_HOME = '/Users/zachary.kourouma/.asdf/installs/java/adoptopenjdk-11.0.26+4'


def bubc [] {
  ^brew upgrade
  ^brew upgrade --greedy
  ^brew cleanup --scrub
}

alias kgp = kubectl get pods


