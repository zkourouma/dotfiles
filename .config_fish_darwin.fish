
if status is-interactive
    function bubc
        command brew upgrade
        command brew upgrade --greedy
        command brew cleanup --scrub
    end

    function kgp
        command kubectl get pods $argv
    end

end

if status is-login
    if test $HOME/.work-tokens.fish
        source $HOME/.work-tokens.fish
    end

    if test $HOME/.ssi/bin
        fish_add_path $HOME/.ssi/bin
    end

    fish_add_path --prepend --move /opt/homebrew/opt/mysql-client/bin
    fish_add_path --prepend --move /usr/local/bin /usr/local/opt/go/libexec/bin
    fish_add_path --prepend --move /opt/homebrew/opt/libpq/bin /opt/homebrew/libiconv/bin /opt/homebrew/opt/curl/bin /opt/homebrew/bin /opt/homebrew/sbin 
    fish_add_path --prepend --move $HOME/.local/bin $HOME/.cargo/bin /usr/local/sbin $HOME/go/third/bin $HOME/.config/emacs/bin $HOME/.ghcup/bin $HOME/.cabal/bin
    fish_add_path --prepend --move $HOME/.asdf/shims $HOME/.pyenv/shims/python
    fish_add_path --prepend --move /run/current-system/sw/bin

    set -gx LDFLAGS -L/opt/homebrew/opt/curl/lib
    set -gx CPPFLAGS -I/opt/homebrew/opt/curl/include
    set -gx LDFLAGS -L/opt/homebrew/opt/libiconv/lib
    set -gx CPPFLAGS -I/opt/homebrew/opt/libiconv/include
    set -gx LIBRARY_PATH $LIBRARY_PATH:$(brew --prefix)/lib:$(brew --prefix)/opt/libiconv/lib
end


