
def pacman-mirrors [] {
    sudo -s reflector -c US,BR,DE,CH,SE,NO,DK,FI,NL,RU,TW,IN --sort score -a 24 -p https,ftp --save /etc/pacman.d/mirrorlist
}

def pacman-upgrade [] {
    pacman-mirrors
    ^paru -Syyuu
}

def pacman-cleanup [] {
    try { ^paru -Qtdqq | ^paru -Rns - }
    ^paru -Sc
}


$env.PATH = ($env.PATH | split row (char esep) | append "/run/current-system/sw/bin")

$env.PATH = ($env.PATH | split row (char esep) | append "/usr/local/bin")
$env.PATH = ($env.PATH | split row (char esep) | append "/usr/local/sbin")
$env.PATH = ($env.PATH | split row (char esep) | append "/usr/local/share/python")
$env.PATH = ($env.PATH | split row (char esep) | append "/usr/local/opt/go/libexec/bin")

$env.PATH = ($env.PATH | split row (char esep) | append ($env.HOME | path join ".asdf" "shims"))
$env.PATH = ($env.PATH | split row (char esep) | append ($env.HOME | path join ".asdf" "bin"))

$env.PATH = ($env.PATH | split row (char esep) | append ($env.HOME | path join ".local" "bin"))
$env.PATH = ($env.PATH | split row (char esep) | append ($env.HOME | path join ".config" "emacs" "bin"))

$env.PATH = ($env.PATH | split row (char esep) | append ($env.HOME | path join ".cargo" "bin"))
$env.PATH = ($env.PATH | split row (char esep) | append ($env.HOME | path join ".ghcup" "bin"))
$env.PATH = ($env.PATH | split row (char esep) | append ($env.HOME | path join ".cabal" "bin"))
$env.PATH = ($env.PATH | split row (char esep) | append ($env.HOME | path join "go" "third" "bin"))

