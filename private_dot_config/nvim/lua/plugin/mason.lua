local ensure_installed = {
  "bashls",
  "lua_ls",
  "markdown_oxide",
  "rust_analyzer",
  "taplo",
  "ts_ls",
  "vimls",
}

return {
  {
    "williamboman/mason.nvim", -- LSP/DAP/Tool installer
    config = function(_, opts)
      require("mason").setup(opts)
    end,
  },
  {
    "williamboman/mason-lspconfig.nvim", -- Mason integration with nvim-lspconfig
    opts = {
      ensure_installed = ensure_installed,
      automatic_installation = true,
    },
    dependencies = {
      "williamboman/mason.nvim",
      "neovim/nvim-lspconfig",
    },
  },
  {
    "jay-babu/mason-null-ls.nvim",
    event = { "BufReadPre", "BufNewFile" },
    dependencies = {
      "williamboman/mason.nvim",
      "nvimtools/none-ls.nvim",
    },
    opts = {
      ensure_installed = ensure_installed,
      automatic_installation = true,
      handlers = {},
    }
  }
}
