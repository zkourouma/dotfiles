return {
  "neovim/nvim-lspconfig", -- Built-in LSP
  dependencies = { 'saghen/blink.cmp' },
  opts = {
    inlay_hints = { enabled = true }
  },
  config = function()
    local capabilities = require('blink.cmp').get_lsp_capabilities()
    local lsp_config = require('lspconfig')

    local lsp_formatting = function(bufnr)
      vim.lsp.buf.format({
        -- filter = function(client)
        --   -- apply whatever logic you want (in this example, we'll only use null-ls)
        --   return client.name == "null-ls"
        -- end,
        bufnr = bufnr,
      })
    end

    -- if you want to set up formatting on save, you can use this as a callback
    local augroup = vim.api.nvim_create_augroup("LspFormatting", {})

    -- add to your shared on_attach callback
    local on_attach = function(client, bufnr)
      if client.supports_method("textDocument/formatting") then
        vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
        vim.api.nvim_create_autocmd("BufWritePre", {
          group = augroup,
          buffer = bufnr,
          callback = function()
            lsp_formatting(bufnr)
          end,
        })
      end
      vim.lsp.inlay_hint.enable(true, { bufnr = bufnr })
    end

    -- Set updatetime for CursorHold
    -- 300ms of no cursor movement to trigger CursorHold
    vim.opt.updatetime = 100
    -- Show diagnostic popup on cursor hover
    local diag_float_grp = vim.api.nvim_create_augroup("DiagnosticFloat", { clear = true })
    vim.api.nvim_create_autocmd("CursorHold", {
      callback = function()
        vim.diagnostic.open_float(nil, { focusable = false })
      end,
      group = diag_float_grp,
    })

    -- have a fixed column for the diagnostics to appear in
    -- this removes the jitter when warnings/errors flow in
    vim.wo.signcolumn = "yes"

    lsp_config.lua_ls.setup {
      capabilities = capabilities,
      settings = {
        Lua = {
          diagnostics = {
            globals = { 'vim' }
          }
        }
      },
      on_attach = on_attach,
      flags = {
        debounce_text_changes = 150,
      },
    }

    lsp_config.dockerls.setup {
      settings = {
        docker = {
          languageserver = {
            formatter = {
              ignoreMultilineInstructions = true,
            },
          },
        }
      }
    }

    lsp_config.eslint.setup({
      capabilities = capabilities,

      on_attach = function(_, bufnr)
        vim.api.nvim_create_autocmd("BufWritePre", {
          buffer = bufnr,
          command = "EslintFixAll",
        })
      end,
    })

    lsp_config.fish_lsp.setup {
      capabilities = capabilities,
      on_attach = on_attach,
      flags = {
        debounce_text_changes = 150,
      },

    }

    lsp_config.hls.setup {
      capabilities = capabilities,
      filetypes = { 'haskell', 'lhaskell', 'cabal' },
      on_attach = on_attach,
      flags = {
        debounce_text_changes = 150,
      },
    }

    lsp_config.markdown_oxide.setup {
      capabilities = capabilities,
      on_attach = on_attach,
      flags = {
        debounce_text_changes = 150,
      },

    }

    lsp_config.nixd.setup {
      capabilities = capabilities,
      on_attach = on_attach,
      flags = {
        debounce_text_changes = 150,
      },
      settings = {
        nixd = {
          nixpkgs = {
            expr = "import <nixpkgs> { }",
          },
          formatting = {
            command = { "alejandra" },
          },
        },
      },
    }

    -- nvim_lsp.postgres_lsp.setup {
    --   capabilities = capabilities,
    --   on_attach = on_attach,
    --   flags = {
    --     debounce_text_changes = 150,
    --   },
    -- }

    local function get_default_features_from_cargo_toml()
      local cargo_toml_path = vim.fn.getcwd() .. '/Cargo.toml'
      if vim.fn.filereadable(cargo_toml_path) == 0 then
        return nil
      end

      local lines = vim.fn.readfile(cargo_toml_path)
      for _, line in ipairs(lines) do
        local feature_match = line:match('%s*default%s*=%s*%[(.-)%]')
        if feature_match then
          return vim.split(feature_match:gsub('"', ''), '%s*,%s*')
        end
      end

      return nil
    end

    local function get_features_from_projectile()
      local projectile_path = vim.fn.getcwd() .. '/.projectile'
      if vim.fn.filereadable(projectile_path) == 0 then
        return nil
      end

      local lines = vim.fn.readfile(projectile_path)
      for _, line in ipairs(lines) do
        local feature_match = line:match('%s*features%s*=%s*%[(.-)%]')
        if feature_match then
          return vim.split(feature_match:gsub('"', ''), '%s*,%s*')
        end
      end

      return nil
    end

    local features = get_default_features_from_cargo_toml() or get_features_from_projectile()

    lsp_config.rust_analyzer.setup {
      capabilities = capabilities,
      settings = {
        ['rust-analyzer'] = {
          diagnostics = {
            enable = true,
            styleLints = {
              enable = true,
            },
          },
          checkOnSave = {
            command = 'clippy'
          },
          cargo = {
            features = features and table.concat(features, ',') or 'all',
            extraArgs = '--workspace',
          },
          inlayHints = {
            lifetimeElisionHints = {
              useParameterNames = true,
              -- Enable hints for small scopes (single line, nested blocks and parameters)
              showSmallScope = true,
              -- Chop off leading pipes
              showPreTyPluralBounds = true,
            },
            bindingModeHints = {
              enable = true,
            },
          },
          procMacro = {
            enable = true,
          },
        },
      },
      on_attach = on_attach,
      commands = {
        ExpandMacro = {
          function()
            vim.lsp.buf_request_all(0, "rust-analyzer/expandMacro", vim.lsp.util.make_position_params(), vim.print)
          end
        }
      },
      flags = {
        debounce_text_changes = 150,
      },
    }

    lsp_config.terraformls.setup {
      on_attach = on_attach,
      flags = {
        debounce_text_changes = 150,
      },
    }

    lsp_config.taplo.setup {
      on_attach = on_attach,
      flags = {
        debounce_text_changes = 150,
      },
    }

    lsp_config.ts_ls.setup {
      on_attach = function(client, bufnr)
        client.server_capabilities.documentFormattingProvider = false
        client.server_capabilities.documentRangeFormattingProvider = false
      end,
      flags = {
        debounce_text_changes = 150,
      },
    }

    lsp_config.yamlls.setup {
      on_attach = on_attach,
      flags = {
        debounce_text_changes = 150,
      },
    }
  end,
}
