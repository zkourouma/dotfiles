local actions = require("fzf-lua").actions

return {
  "ibhagwan/fzf-lua",
  -- optional for icon support
  dependencies = { "nvim-tree/nvim-web-devicons" },
  -- or if using mini.icons/mini.nvim
  -- dependencies = { "echasnovski/mini.icons" },
  opts = {
    'ivy',
    fzf_colors = true,
    resume = true,
    file_icon_padding = ' ',
    actions = {
      files = {
        ['enter'] = actions.file_switch_or_edit,
        ['alt-i'] = actions.toggle_ignore,
        ['alt-h'] = actions.toggle_hidden
      }
    },
    files = {
      follow = true
    }
  }
}
