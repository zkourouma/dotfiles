return {
  "nvimtools/none-ls.nvim", -- LSP features for non-LSP sources (e.g., linters, formatters)
  dependencies = "nvimtools/none-ls-extras.nvim",
  config = function()
    local none_ls = require("null-ls")
    none_ls.setup({
      sources = {
        -- none_ls.builtins.formatting.stylua,
        none_ls.builtins.completion.spell,
        none_ls.builtins.diagnostics.alex,
        -- sh
        none_ls.builtins.diagnostics.fish,
        -- rust-lang
        -- none_ls.sources.diagnostics.clippy.with({
        --   extra_args = {"--no-deps"},
        -- }),
        require("none-ls.formatting.rustfmt"),
        require("none-ls.formatting.jq"),
        none_ls.builtins.diagnostics.yamllint,
        none_ls.builtins.formatting.prettier.with({
          filetypes = { "typescript", "javascript", "json" },
        }),

        -- git
        -- none_ls.builtins.code_actions.gitsigns,
        -- none_ls.builtins.diagnostics.commitlint,
      },
      -- you can reuse a shared lspconfig on_attach callback here
      on_attach = function(client, bufnr)
        local augroup = vim.api.nvim_create_augroup("LspFormatting", {})
        if client.supports_method("textDocument/formatting") then
          vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
          vim.api.nvim_create_autocmd("BufWritePre", {
            group = augroup,
            buffer = bufnr,
            callback = function()
              -- on 0.8, you should use vim.lsp.buf.format({ bufnr = bufnr }) instead
              -- on later neovim version, you should use vim.lsp.buf.format({ async = false }) instead
              vim.lsp.buf.format({ async = false, timeout_ms = 5000 })
            end,
          })
        end
      end,
    })
  end,
}
