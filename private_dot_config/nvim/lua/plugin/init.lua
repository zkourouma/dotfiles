return {
  -- themes
  "RRethy/base16-nvim",
  {
    "Vallen217/eidolon.nvim",
    lazy = false,
    priority = 1000,
  },
  -- lsp
  {
    "folke/trouble.nvim",
    opts = {
      auto_close = true,
    }, -- for default options, refer to the configuration section for custom setup.
    cmd = "Trouble",
  },
  { "j-hui/fidget.nvim", opts = {}, },
  "mfussenegger/nvim-dap",
  {
    "rcarriga/nvim-dap-ui",
    dependencies = { "mfussenegger/nvim-dap", "nvim-neotest/nvim-nio" }
  },
  -- "reasonml/vim-reason-loader",
  -- "hasufell/ghcup.vim",
  -- "slashmili/alchemist.vim"
  -- "ElmCast/elm-vim",
  -- "neovimhaskell/haskell-vim",
  -- "cespare/vim-toml",
  -- "stephpy/vim-yaml",
  -- "rust-lang/rust.vim",
  -- "elzr/vim-json",
  -- "plasticboy/vim-markdown",
  -- "LnL7/vim-nix",
  {
    'LhKipp/nvim-nu',
    build = ':TSInstall nu',
    opts = {}
  },
  { "cordx56/rustowl",   dependencies = { "neovim/nvim-lspconfig" } },

  -- UI
  "godlygeek/tabular",
  { "lukas-reineke/indent-blankline.nvim", main = "ibl", opts = {} },

  -- cli utils
  "junegunn/fzf",

  -- "ahmedkhalf/project.nvim",
  "groenewege/vim-less",
  "nvim-pack/nvim-spectre",

  -- convenience helpers
  "tpope/vim-commentary",
  "tpope/vim-surround",
  "tpope/vim-repeat",
  {
    "folke/lazydev.nvim",
    ft = "lua", -- only load on lua files
    opts = {
      library = {
        -- See the configuration section for more details
        -- Load luvit types when the `vim.uv` word is found
        { path = "luvit-meta/library", words = { "vim%.uv" } },
      },
    },
  },
  -- git
  "tpope/vim-fugitive",
  "shumphrey/fugitive-gitlab.vim",
  "lewis6991/gitsigns.nvim",
  {
    "NeogitOrg/neogit",
    dependencies = {
      "nvim-lua/plenary.nvim",  -- required
      "sindrets/diffview.nvim", -- optional - Diff integration

      -- Only one of these is needed, not both.
      -- "nvim-telescope/telescope.nvim", -- optional
      "ibhagwan/fzf-lua", -- optional
    },
    config = true
  }
}
