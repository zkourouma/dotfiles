local ensure_installed = {
  "c",
  "lua",
  "query",
  "vim",
  "vimdoc",

  "bash",
  "commonlisp",
  "diff",
  "dockerfile",
  "fish",
  "git_config",
  "git_rebase",
  "gitcommit",
  "gitattributes",
  "gitignore",
  "graphql",
  "haskell",
  "html",
  "javascript",
  "jsonc",
  "make",
  "markdown_inline",
  "mermaid",
  "nix",
  "regex",
  "rust",
  "sql",
  "ssh_config",
  "terraform",
  "tmux",
  "toml",
  "typescript",
  "yaml"
}

return {
  {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    opts = {
      ensure_installed = ensure_installed,
      sync_install = false,
      highlight = { enable = true },
      indent = { enable = true },
    },
    config = function(_, opts)
      require("nvim-treesitter.configs").setup(opts)
    end
  },

  { 'nvim-treesitter/nvim-treesitter-textobjects', dependencies = 'nvim-treesitter/nvim-treesitter' },
  { 'nvim-treesitter/nvim-treesitter-refactor',    dependencies = 'nvim-treesitter/nvim-treesitter' },
  { 'windwp/nvim-ts-autotag',                      dependencies = 'nvim-treesitter/nvim-treesitter' },
}
