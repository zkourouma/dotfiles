local function map(mode, k, v)
  vim.keymap.set(mode, k, v, { silent = true, noremap = true })
end


map('n', '<leader>fs', '<CMD>update<CR>')

map('n', 'gh', '<CMD>lua vim.lsp.buf.hover()<CR>')
map('n', '<leader>ca', '<CMD>lua vim.lsp.buf.code_action()<CR>')
map('n', '<leader>cf', '<CMD>lua vim.lsp.buf.format()<CR>')
map('n', '<leader>cr', '<CMD>lua vim.lsp.buf.rename()<CR>')

map('n', '<leader>F', '<CMD>lua require("spectre").toggle()<CR>')
map('n', '<leader>C', '<CMD>lua require("spectre").open_visual({select_word=true})<CR>')
map('n', '<leader>bd', '<CMD>bdelete<CR>')

map('n', '-', '<CMD>Oil<CR>')

map('n', '<leader>c.', '<CMD>lua vim.diagnostic.open_float()<CR>')

map('n', '<leader>xX', '<CMD>Trouble diagnostics toggle<CR>')
map('n', '<leader>cs', '<CMD>Trouble symbols toggle focus=false<CR>')
map('n', '<leader>cl', '<CMD>Trouble lsp toggle focus=false win.position=right<CR>')
map('n', '<leader>xL', '<CMD>Trouble loclist toggle<CR>')
map('n', '<leader>xQ', '<CMD>Trouble qflist toggle<CR>')
map('n', 'g]', '<CMD>lua vim.diagnostic.goto_next()<CR>')
map('n', 'g[', '<CMD>lua vim.diagnostic.goto_prev()<CR>')

local fzfLua = require('fzf-lua')
map('n', 'gd', function() fzfLua.lsp_definitions({ jump1 = true }) end)
map('n', '<leader>ff', fzfLua.files)
map('n', '<leader>fh', fzfLua.help_tags)
map('n', '<leader>cp', function() fzfLua.files({ cwd = '~/.local/share/chezmoi/' }) end)
map('n', '<leader>fx', fzfLua.quickfix)
map('n', '<leader>bb', fzfLua.buffers)
map('n', '<leader>/', fzfLua.live_grep)
-- map('n', '<leader>/', fzfLua.live_grep_glob)
map('n', '<leader>fl', fzfLua.live_grep_resume)
map('n', '<leader>*', fzfLua.grep_cword)
map('n', '<leader>cx', fzfLua.diagnostics_document)
map('n', '<leader>cX', fzfLua.diagnostics_workspace)
map('n', 'gr', function() fzfLua.lsp_references({ ignore_current_line = true, includeDeclaration = false }) end)
map('n', '<leader>gc', fzfLua.git_bcommits)
map('n', '<leader>gC', fzfLua.git_commits)
map('n', '<leader>gbl', fzfLua.git_blame)
map('n', '<leader>gbr', fzfLua.git_branches)

local config = require("fzf-lua.config")
local actions = require("trouble.sources.fzf").actions
config.defaults.actions.files["ctrl-t"] = actions.open

local dropbar_api = require('dropbar.api')
vim.keymap.set('n', '<leader>;', dropbar_api.pick, { desc = 'Pick symbols in winbar' })
vim.keymap.set('n', '[;', dropbar_api.goto_context_start, { desc = 'Go to start of current context' })
vim.keymap.set('n', '];', dropbar_api.select_next_context, { desc = 'Select next context' })
