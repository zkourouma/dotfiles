export use ./adb-completions.nu *
export use ./bat-completions.nu *
export use ./cargo-completions.nu *
export use ./curl-completions.nu *
export use ./fastboot-completions.nu *
export use ./git-completions.nu *
export use ./rg-completions.nu *
export use ./rustup-completions.nu *

export use ./jj-completions.nu *
