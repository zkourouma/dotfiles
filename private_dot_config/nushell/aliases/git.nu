
def git_current_branch [] {
  ^git branch --show-current | str trim -c "\n"
}

export alias gapa = ^git add --patch

export alias gco = ^git checkout
export alias gcb = ^git checkout -b

export alias gb = ^git branch
export alias gbD = ^git branch -D

export alias gc = ^git commit --verbose
export alias gc! = ^git commit --verbose --amend
export alias gcmsg = ^git commit --verbose --message

export alias gd = ^git diff
export alias gdca = ^git diff --cached

export alias gfa = ^git fetch --all --prune --tags
export alias gl = ^git pull origin
export alias gupa = ^git pull --rebase --autostash --verbose origin

export alias glgg = ^git log --oneline --decorate --color --graph
export alias glgga = ^git log --oneline --decorate --color --graph --all

export alias gp = ^git push origin
export alias gpf = ^git push --force-with-lease origin
export def gpsup [] {
  let branch = (git_current_branch)
  ^git push --set-upstream origin $branch
}

export alias gst = ^git status


export alias grb = git rebase
export alias grba = ^git rebase --abort
export alias grbc = ^git rebase --continue
export alias grbd = ^git rebase origin/develop

export alias grhh = ^git reset --hard
export alias grhs = ^git reset --soft

export alias grm = ^git rm
export alias grmc = ^git rm --cached

export alias gbs = ^git bisect
export alias gbsb = ^git bisect bad
export alias gbsg = ^git bisect good

export def grs [staged_file?: string] {
  match $staged_file {
    null => {
      ^git restore Cargo.lock;
    },
    _ => {
      ^git restore $staged_file;
    }
  }
}

export def grst [staged_file?: string] {
  match $staged_file {
    null => {
      ^git restore --staged Cargo.lock;
    },
    _ => {
      ^git restore --staged $staged_file;
    }
  }
}
