export use ./git.nu *
export use ./tmux.nu *

export alias core-cat = cat
export alias cat = ^bat

export alias core-diff = diff
export alias diff = ^delta

export alias core-ping = ping
export alias ping = ^prettyping --nolegend

export alias core-du = du
export alias du = ^ncdu --color dark -x --exclude .git --exclude node_modules --exclude .stack-work --exclude elm-stuff

export alias ll = ls -a

export alias cm = ^chezmoi

export alias stack-default = ^stack test --fast --haddock-deps --file-watch

export def rustup-cleanup [toolchain?: string] {
  match $toolchain {
    null => {
      ^rustup toolchain list | each {|tc| ^rustup toolchain uninstall $tc}
    },
    _ => {
      ^rustup toolchain uninstall $toolchain
    },
  }
}

export def asdf-upgrade [] {
  HOME=$env.HOME ^asdf plugin update --all
}

def nix-upgrade [] {
  ^nix-collect-garbage -d
  ^nix flake update --flake {{ .nix_flake_path }}
  ^{{ .nix_manager }} switch --flake {{ .nix_flake_path }}
}

export def cargo-binstall-upgrade [] {
  ^cargo install --list | lines | where { |x| not ($x | str starts-with '    ') } | parse "{name} v{version}:" | get name | each { |name| cargo binstall $name --no-confirm }
}
