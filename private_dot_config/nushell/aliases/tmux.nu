export alias ts = ^tmux new -s
export alias ta = ^tmux attach-session
export alias tksv = ^tmux kill-server
export alias tkss = ^tmux kill-session
