;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "zack kourouma"
      user-mail-address "zack@kourouma.me")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
(cond ((eq system-type 'darwin)
       (setq doom-font (font-spec :family "0xProto" :size 12 :weight 'regular)
             doom-variable-pitch-font (font-spec :family "Atkinson Hyperlegible" :size 13))
       )
      ((eq system-type 'gnu/linux)
       (setq doom-font (font-spec :family "0xProto" :size 24 :weight 'semi-light)
             doom-variable-pitch-font (font-spec :family "Atkinson Hyperlegible" :size 26))
       ))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-tokyo-night)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

(setq confirm-kill-emacs nil)
;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
(setq tramp-default-method "ssh")

(setq shell-file-name (executable-find "zsh"))
(setq-default vterm-shell (executable-find "fish"))
(setq-default explicit-shell-file-name (executable-find "fish"))

(setq vertico-resize t)
(setq vertico-cycle t)

(use-package emacs
  :init
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
  (defun crm-indicator (args)
    (cons (format "[CRM%s] %s"
                  (replace-regexp-in-string
                   "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
                   crm-separator)
                  (car args))
          (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal buffers.
  ;; (setq read-extended-command-predicate
  ;;       #'command-completion-default-include-p)

  ;; Enable recursive minibuffers
  (setq enable-recursive-minibuffers t))
;; Configure tree-sitter language sources
(setq treesit-language-source-alist
      '((bash "https://github.com/tree-sitter/tree-sitter-bash")
        (dockerfile "https://github.com/camdencheek/tree-sitter-dockerfile")
        (elisp "https://github.com/Wilfred/tree-sitter-elisp")
        (fish "https://github.com/ram02z/tree-sitter-fish")
        (gitattributes "https://github.com/tree-sitter-grammars/tree-sitter-gitattributes")
        (gitignore "https://github.com/shunsambongi/tree-sitter-gitignore")
        (haskell "https://github.com/tree-sitter/tree-sitter-haskell")
        (javascript "https://github.com/tree-sitter/tree-sitter-javascript" "master" "src")
        (jq "https://github.com/flurie/tree-sitter-jq")
        (json "https://github.com/tree-sitter/tree-sitter-json")
        (make "https://github.com/alemuller/tree-sitter-make")
        (markdown "https://github.com/ikatyang/tree-sitter-markdown")
        (regex "https://github.com/tree-sitter/tree-sitter-regex")
        (rust "https://github.com/tree-sitter/tree-sitter-rust")
        (sql "https://github.com/m-novikov/tree-sitter-sql")
        (toml "https://github.com/tree-sitter/tree-sitter-toml")
        (typescript "https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src")
        (yaml "https://github.com/ikatyang/tree-sitter-yaml")))

;; (add-to-list 'tree-sitter-major-mode-language-alist '(rustic-mode . rust))

;; Configure directory extension.
(use-package vertico-directory
  :after vertico
  ;; More convenient directory navigation commands
  :bind (:map vertico-map
              ("RET" . vertico-directory-enter)
              ("DEL" . vertico-directory-delete-char)
              ("M-DEL" . vertico-directory-delete-word))
  ;; Tidy shadowed file names
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))

;; (setq mini-frame-mode t)
;; (custom-set-variables
;;  '(mini-frame-show-parameters
;;    '((top . 10)
;;      (width . 0.7)
;;      (left . 0.5))))

(setq completion-styles '(orderless basic)
      completion-category-defaults nil
      completion-category-overrides '((file (styles partial-completion))))

;; Enable rich annotations using the Marginalia package
(use-package marginalia
  ;; Either bind `marginalia-cycle' globally or only in the minibuffer
  :bind (("M-A" . marginalia-cycle)
         :map minibuffer-local-map
         ("M-A" . marginalia-cycle))

  ;; The :init configuration is always executed (Not lazy!)
  :init
  ;; Must be in the :init section of use-package such that the mode gets
  ;; enabled right away. Note that this forces loading the package.
  (marginalia-mode)
  :config
  (marginalia-mode))
(all-the-icons-completion-mode)
(add-hook 'marginalia-mode-hook #'all-the-icons-completion-marginalia-setup)

(add-hook 'dired-mode-hook 'all-the-icons-dired-mode)

(use-package embark
  :bind
  (("C-." . embark-act)         ;; pick some comfortable binding
   ("C-;" . embark-dwim)        ;; good alternative: M-.
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'

  :init
  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)

  ;; Show the Embark target at point via Eldoc.  You may adjust the Eldoc
  ;; strategy, if you want to see the documentation from multiple providers.
  (add-hook 'eldoc-documentation-functions #'embark-eldoc-first-target)
  ;; (setq eldoc-documentation-strategy #'eldoc-documentation-compose-eagerly)

  :config
  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none)))))

;; Consult users will also want the embark-consult package.
(use-package embark-consult
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(use-package centaur-tabs
  :demand
  :config
  (centaur-tabs-mode t))
(setq centaur-tabs-style "chamfer")
(setq centaur-tabs-set-icons t)
(setq centaur-tabs-set-bar 'left)
;; (setq x-underline-at-descent-line t) ;; required for 'under bar
;; (define-key evil-normal-state-map (kbd "g T") 'centaur-tabs-backward)
;; (define-key evil-normal-state-map (kbd "g t") 'centaur-tabs-forward)
(centaur-tabs-change-fonts "0xProto" 140)
(centaur-tabs-enable-buffer-reordering)
(centaur-tabs-group-by-projectile-project)

(setq compilation-scroll-output t)

(add-hook 'magit-mode-hook (lambda () (magit-delta-mode +1)))

(use-package lsp-ui
  :init
  (setq lsp-ui-doc-enable t)
  (setq lsp-ui-doc-position 'at-point))


(use-package lsp
  :init
  (setq lsp-inlay-hint-enable t))

(use-package lsp-ui-peek
  :config
  (define-key lsp-ui-mode-map [remap xref-find-definitions] #'lsp-ui-peek-find-definitions)
  (define-key lsp-ui-mode-map [remap xref-find-references] #'lsp-ui-peek-find-references))

(after! lsp-mode
  (map! :n "gh" #'lsp-ui-doc-show)
  (map! :n "gr" #'lsp-find-references))

(add-hook 'rust-mode-hook
          (lambda () (setq indent-tabs-mode nil)))
(add-hook 'rust-mode-hook 'cargo-minor-mode)
(setq lsp-rust-analyzer-display-chaining-hints t)
(setq lsp-rust-analyzer-display-lifetime-elision-hints-enable "skip_trivial")
(setq lsp-rust-analyzer-display-parameter-hints t)
(setq lsp-rust-analyzer-server-display-inlay-hints t)
(setq rustic-rustfmt-args "+nightly")
(setq rustic-default-clippy-arguments "--workspace --benches --tests --all-features")

(setq lsp-haskell-formatting-provider 'fourmolu)

(add-hook 'conf-toml-mode-hook #'lsp!) ;; Enable LSP mode for TOML files
(add-hook 'conf-toml-mode-hook
          (lambda ()
            (add-hook 'before-save-hook #'lsp-format-buffer nil t)))

(use-package prettier
  :hook ((typescript-mode . prettier-mode)
         (js-mode . prettier-mode)
         (json-mode . prettier-mode)
         (yaml-mode . prettier-mode))
  :init (setq prettier-inline-errors-flag t))

(use-package nix-mode
  :ensure t
  :hook
  (nix-mode . lsp-deferred) ;; So that direnv integration with `envrc-mode' will work
  :config
  (setq lsp-nix-nixd-server-path "nixd")) ;; Note: make sure that nil is not in your $PATH

;; For Tree Sitter
(use-package nix-ts-mode
  :ensure t
  :mode "\\.nix\\'"
  :hook
  (nix-ts-mode . lsp-deferred) ;; So that envrc mode will work
  :config
  (setq lsp-nix-nixd-server-path "nixd")) ;; Note: make sure that nil is not in your $PATH

