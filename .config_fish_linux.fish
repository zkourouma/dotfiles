
if status is-interactive
    # Commands to run in interactive sessions can go here
    function pacman-mirrors
        command sudo -s reflector -c US,DE,CH,SE,NO,DK,FI,NL,RU,TW,IN --sort score -a 24 -p https,ftp --save /etc/pacman.d/mirrorlist
    end

    function pacman-upgrade
        pacman-mirrors
        command paru -Syyuu
    end

    function pacman-cleanup
        command paru -Qtdqq | command paru -Rns -
        command paru -Sc
    end
end

if status is-login
    fish_add_path $HOME/.local/bin $HOME/.asdf/shims $HOME/.asdf/bin $HOME/.cargo/bin /usr/local/sbin /usr/local/bin/git /usr/local/bin /usr/local/share/python/ /usr/local/opt/go/libexec/bin $HOME/.config/emacs/bin $HOME/.ghcup/bin $HOME/.cabal/bin $HOME/go/third/bin
    fish_add_path --prepend --move /run/current-system/sw/bin
end


